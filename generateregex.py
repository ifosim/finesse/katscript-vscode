import json
from finesse.script.spec import KatSpec
from finesse.script.tokens import NUMBER

spec = KatSpec()

commands = [*spec.commands, *spec.analyses]
commands.sort(key = lambda item: (-len(item), item))

elements = list(spec.elements)
elements.sort(key = lambda item: (-len(item), item))

functions = list(spec.expression_functions)
functions.sort(key = lambda item: (-len(item), item))

keywords = list(spec.reserved_names)
keywords.sort(key = lambda item: (-len(item), item))

operators = list({*spec.binary_operators, *spec.unary_operators})
# "&" isn't really treated like an operator in KatScript, but highlight it as one
operators.append("&")
# The only multi-character operators are repetitions of the same character, so we remove them to
# allow square brackets in regex to work.
for x in [op for op in operators if len(op) > 1]:
    operators.remove(x)

# "-" has to appear at the end of the list, as all these operators will go into square brackets in
# a regex, and "-" in the middle of square brackets implies a range.
operators.remove("-")
operators.append("-")


with open("template.json") as file:
    template = json.load(file)

# Only match commands and elements at the beginning of the line
template["repository"]["commands"]["patterns"][0]["match"] = fr"^\s*\b({'|'.join(commands)})\b"
template["repository"]["elements"]["patterns"][0]["match"] = fr"^\s*\b({'|'.join(elements)})\b"

# In addition to word-boundaries, functions and keywords shouldn't be matched if the previous
# character was a ".", to avoid matching attributes of components
template["repository"]["functions"]["patterns"][0]["match"] = fr"(?<!\.)\b({'|'.join(functions)})\b"
template["repository"]["keywords"]["patterns"][0]["match"] = fr"(?<!\.)\b({'|'.join(keywords)})\b"

# Simple list for operators
template["repository"]["operators"]["patterns"][0]["match"] = fr"[{''.join(operators)}]"

# Numbers should only match if they don't come after a word character
template["repository"]["numbers"]["patterns"][0]["match"] = fr"(?<!\w){NUMBER}"

with open("katscript/katscript.tmLanguage.json", "w") as file:
    file.write(json.dumps(template, indent=4))
